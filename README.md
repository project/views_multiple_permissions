Description
-----------

Provides a Views access control plugin that allows to check against multiple
permissions. By default, Drupal core Views module only allows to perform the
access check against a single permission. The selected permissions logic can be
also configured to grant the access only if <em>all permissions</em> are passing
(AND logic) or if <em>any permission</em> passes (OR logic).
