<?php

namespace Drupal\Tests\views_multiple_permissions\Functional;

use Drupal\Core\Serialization\Yaml;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the multiple permissions Views access plugin.
 *
 * @group views_multiple_permissions
 */
class ViewsMultiplePermissionsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'views_multiple_permissions',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the multiple permissions Views access plugin.
   */
  public function testViewsMultiplePermissions(): void {
    $assert = $this->assertSession();
    /** @var \Drupal\Core\Routing\RouteBuilderInterface $route_builder */
    $route_builder = \Drupal::service('router.builder');

    $config = $this->config('views.view.test_views_multiple_permissions');
    $config->setData(Yaml::decode(file_get_contents(__DIR__ . '/../../fixtures/views.view.test_views_multiple_permissions.yml')))->save();
    $route_builder->rebuild();

    // Check that anonymous cannot access the view.
    $this->drupalGet('/test_views_multiple_permissions');
    $assert->statusCodeEquals(403);

    // Check that a regular user cannot access the view.
    $this->drupalLogin($this->createUser());
    $this->drupalGet('/test_views_multiple_permissions');
    $assert->statusCodeEquals(403);

    // Check that OR operator works with only one permission.
    $this->drupalLogin($this->createUser([
      'administer modules',
      // This permission is not configured in the plugin.
      'access site in maintenance mode',
    ]));
    $this->drupalGet('/test_views_multiple_permissions');
    $assert->statusCodeEquals(200);

    // Check that OR operator works with all permissions.
    $this->drupalLogin($this->createUser([
      'administer modules',
      'administer themes',
      // This permission is not configured in the plugin.
      'access site in maintenance mode',
    ]));
    $this->drupalGet('/test_views_multiple_permissions');
    $assert->statusCodeEquals(200);

    // Switch to 'AND logic'.
    $config->set('display.default.display_options.access.options.operator', 'and')->save();
    $route_builder->rebuild();

    // Check that AND operator doesn't work when some permissions are missed.
    $this->drupalLogin($this->createUser([
      'administer modules',
      // This permission is not configured in the plugin.
      'access site in maintenance mode',
    ]));
    $this->drupalGet('/test_views_multiple_permissions');
    $assert->statusCodeEquals(403);

    // Check that AND operator works with all permissions.
    $this->drupalLogin($this->createUser([
      'administer modules',
      'administer themes',
      // This permission is not configured in the plugin.
      'access site in maintenance mode',
    ]));
    $this->drupalGet('/test_views_multiple_permissions');
    $assert->statusCodeEquals(200);
  }

}
