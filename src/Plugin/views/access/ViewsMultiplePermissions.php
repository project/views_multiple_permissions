<?php

namespace Drupal\views_multiple_permissions\Plugin\views\access;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Plugin\views\access\Permission;
use Symfony\Component\Routing\Route;

/**
 * Views access plugin checking against multiple permissions.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "views_multiple_permissions",
 *   title = @Translation("Multiple Permissions"),
 *   help = @Translation("Access will be granted to users with the specified permissions."),
 * )
 */
class ViewsMultiplePermissions extends Permission {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account): bool {
    foreach ($this->options['permissions'] as $permission) {
      $has_permission = $account->hasPermission($permission);
      if ($has_permission && $this->options['operator'] === 'or') {
        return TRUE;
      }
      if (!$has_permission && $this->options['operator'] === 'and') {
        return FALSE;
      }
    }
    return $this->options['operator'] === 'and';
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route): void {
    $glue = $this->options['operator'] === 'and' ? ',' : '+';
    $permission = implode($glue, $this->options['permissions']);
    $route->setRequirement('_permission', $permission);
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle(): MarkupInterface {
    return $this->options['operator'] === 'and' ? $this->t('All permissions are required') : $this->t('At least one permission is required');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    unset($options['perm']);
    $options['permissions'] = ['default' => ['access content']];
    $options['operator'] = ['default' => 'and'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form['permissions'] = [
      '#type' => 'select',
      '#title' => $this->t('Permissions'),
      '#default_value' => $this->options['permissions'],
      '#description' => $this->t('Only users with the selected permissions will be able to access this display.'),
      '#multiple' => TRUE,
      '#size' => 15,
      '#required' => TRUE,
      '#options' => $form['perm']['#options'],
    ];

    $form['operator'] = [
      '#type' => 'radios',
      '#title' => $this->t('How to check permissions'),
      '#options' => [
        'and' => $this->t('All permissions are required (AND logic)'),
        'or' => $this->t('At least one permission is required (OR logic)'),
      ],
      '#default_value' => $this->options['operator'],
      '#required' => TRUE,
    ];

    unset($form['perm']);
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state): void {
    // Normalize the permissions array.
    $key = ['access_options', 'permissions'];
    $form_state->setValue($key, array_values($form_state->getValue($key)));
    parent::submitOptionsForm($form, $form_state);
  }

}
